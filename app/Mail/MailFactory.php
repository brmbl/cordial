<?php

namespace App\Mail;

use App\EmailQueue;
use Illuminate\Mail\Mailable;

class MailFactory
{
    /**
     * @param EmailQueue $queueItem
     *
     * @return Mailable
     */
    public static function createFromQueue(EmailQueue $queueItem): Mailable
    {
        switch ($queueItem->getAttribute('type')) {
            case EmailQueue::TYPE_ACTIVATION :
                $templateName = $queueItem->getAttribute('template');
                $params = json_decode($queueItem->getAttribute('params'), true);

                if (!is_array($params)) {
                    throw new \InvalidArgumentException('Email params contain invalid json');
                }

                return new ActivationCodeMail($templateName, $params);
                break;
        }
    }
}
