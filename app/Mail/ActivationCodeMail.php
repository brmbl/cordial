<?php

namespace App\Mail;

use App\ValueObjects\ActivationCode;
use Illuminate\Mail\Mailable;

class ActivationCodeMail extends Mailable
{
    const TEMPLATE_DEFAULT = 'emails.activationCode';

    /** @var string */
    public $templateName;

    /** @var ActivationCode */
    public $code;

    /**
     * @param string $templateName
     * @param array  $params
     */
    public function __construct(string $templateName, array $params)
    {
        if (!array_key_exists('code', $params)) {
            throw new \InvalidArgumentException('Required parameter code is missing');
        }

        $this->templateName = $templateName;
        $this->code = ActivationCode::fromString($params['code']);
    }

    public function build()
    {
        return $this->view($this->templateName);
    }
}
