<?php

namespace App\Http\Controllers;

use App\DTO\LoginData;
use App\DTO\RegistrationData;
use App\Exceptions\InvalidParameterException;
use App\Exceptions\ValidationException;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /** @var UserService */
    private $userService;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function register(Request $request): JsonResponse
    {
        $this->validateRequestPost($request, ['name', 'email', 'password']);

        $registrationData = RegistrationData::fromScalar(
            $request->post('name'),
            $request->post('email'),
            $request->post('password')
        );

        $userId = $this->userService->register($registrationData);

        return response()->json(['id' => $userId], 201);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function login(Request $request)
    {
        $this->validateRequestPost($request, ['email', 'password']);
        $loginData = LoginData::fromScalar($request->post('email'), $request->post('password'));

        try {
            $authToken = $this->userService->login($loginData);
        } catch (\LogicException $e) {
            //todo: add logging
            throw new \InvalidArgumentException('Username or password is invalid');
        }

        return response()->json(['auth_token' => $authToken]);
    }

    public function restricted()
    {
        return response()->json('Welcome, ' . Auth::user()->name);
    }

    /**
     * @param Request $request
     * @param array   $params
     *
     * @throws ValidationException
     */
    private function validateRequestPost(Request $request, array $params): void
    {
        $requestErrors = [];
        foreach ($params as $param) {
            if (is_null($request->post($param))) {
                $requestErrors[] = new InvalidParameterException(
                    $param,
                    "Required parameter $param is missing"
                );
            }
        }

        if ($requestErrors) {
            throw new ValidationException($requestErrors);
        }
    }
}
