<?php

namespace App\DTO;

use App\Exceptions\InvalidParameterException;
use App\Exceptions\ValidationException;
use App\ValueObjects\EmailAddress;
use App\ValueObjects\Password;

class LoginData
{
    /** @var EmailAddress */
    private $email;

    /** @var Password */
    private $password;

    /**
     * @param EmailAddress $email
     * @param Password     $password
     */
    private function __construct(EmailAddress $email, Password $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * @param string $email
     * @param string $password
     *
     * @return LoginData
     * @throws ValidationException
     */
    public static function fromScalar(string $email, string $password): LoginData
    {
        $errors = [];

        try {
            $email = EmailAddress::fromString($email);
        } catch (\InvalidArgumentException $e) {
            $errors[] = new InvalidParameterException('email', $e->getMessage());
        }

        try {
            $password = Password::fromPlain($password);
        } catch (\InvalidArgumentException $e) {
            $errors[] = new InvalidParameterException('password', $e->getMessage());
        }

        if ($errors) {
            throw new ValidationException($errors);
        }

        return new self($email, $password);
    }

    /**
     * @return EmailAddress
     */
    public function getEmail(): EmailAddress
    {
        return $this->email;
    }

    /**
     * @return Password
     */
    public function getPassword(): Password
    {
        return $this->password;
    }
}
