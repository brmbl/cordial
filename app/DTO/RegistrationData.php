<?php

namespace App\DTO;

use App\Exceptions\InvalidParameterException;
use App\Exceptions\ValidationException;
use App\User;
use App\ValueObjects\EmailAddress;
use App\ValueObjects\Name;
use App\ValueObjects\Password;

class RegistrationData
{
    /** @var Name */
    private $name;

    /** @var EmailAddress */
    private $email;

    /** @var Password */
    private $password;

    /**
     * @param Name         $name
     * @param EmailAddress $email
     * @param Password     $password
     */
    private function __construct
    (
        Name $name,
        EmailAddress $email,
        Password $password
    ) {
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * @param string $name
     * @param string $email
     * @param string $password
     *
     * @return RegistrationData
     * @throws ValidationException
     */
    public static function fromScalar
    (
        string $name,
        string $email,
        string $password
    ): RegistrationData {
        $errors = [];

        try {
            $name = Name::fromString($name);
        } catch (\InvalidArgumentException $e) {
            $errors[] = new InvalidParameterException('name', $e->getMessage());
        }

        try {
            $email = EmailAddress::fromString($email);
        } catch (\InvalidArgumentException $e) {
            $errors[] = new InvalidParameterException('email', $e->getMessage());;
        }

        try {
            $password = Password::fromPlain($password);
        } catch (\InvalidArgumentException $e) {
            $errors[] = new InvalidParameterException('password', $e->getMessage());;
        }

        if (!empty($errors)) {
            throw new ValidationException($errors);
        }

        return new self ($name, $email, $password);
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return EmailAddress
     */
    public function getEmail(): EmailAddress
    {
        return $this->email;
    }

    /**
     * @return Password
     */
    public function getPassword(): Password
    {
        return $this->password;
    }
}
