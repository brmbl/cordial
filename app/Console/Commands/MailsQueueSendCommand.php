<?php

namespace App\Console\Commands;

use App\Workers\EmailWorker;
use Illuminate\Console\Command;

class MailsQueueSendCommand extends Command
{
    /** @var string */
    protected $signature = 'mails-queue:send {batch=10}';

    /** @var string */
    protected $description = 'Send batch of emails from queue';

    /**
     * @param EmailWorker $worker
     */
    public function handle(EmailWorker $worker)
    {
        $worker->handle((int)$this->argument('batch'));
    }
}
