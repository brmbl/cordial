<?php

namespace App;


use Jenssegers\Mongodb\Eloquent\Model;

class AuthToken extends Model
{
    protected $connection = 'mongodb';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'token'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
