<?php

namespace App\ValueObjects;


class EmailAddress
{
    const EMAIL_PATTERN = '/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/';

    /** @var string */
    private $value;

    /**
     * @param string $value
     */
    private function __construct(string $value)
    {
        if (!preg_match(self::EMAIL_PATTERN, $value)) {
            throw new \InvalidArgumentException('Email format is invalid');
        }

        $this->value = $value;
    }

    /**
     * @param string $string
     *
     * @return EmailAddress
     */
    public static function fromString(string $string): EmailAddress
    {
        return new self($string);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }
}
