<?php

namespace App\ValueObjects;


class Name
{
    const MIN_LENGTH = 2;

    const MAX_LENGTH = 15;

    /** @var string */
    private $value;

    /**
     * @param string $value
     */
    private function __construct(string $value)
    {
        if (strlen($value) < self::MIN_LENGTH || strlen($value) > self::MAX_LENGTH) {
            throw new \InvalidArgumentException('Name length should be between '
                . self::MIN_LENGTH . ' and ' . self::MAX_LENGTH . ' characters');
        }

        $this->value = $value;
    }

    /**
     * @param string $string
     *
     * @return Name
     */
    public static function fromString(string $string): Name
    {
        return new self ($string);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }
}
