<?php

namespace App\ValueObjects;

class ActivationCode
{
    /** @var int */
    private $code;

    /**
     * @param int $code
     */
    private function __construct(int $code)
    {
        if (!preg_match('/^[0-9]{4}$/', $code)) {
            throw new \InvalidArgumentException('Activation code format invalid');
        }

        $this->code = $code;
    }

    /**
     * @param string $code
     *
     * @return ActivationCode
     */
    public static function fromString(string $code): ActivationCode
    {
        return new self((int)$code);
    }

    /**
     * @return ActivationCode
     */
    public static function generate(): ActivationCode
    {
        return new self(rand(1000, 9999));
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->code;
    }
}
