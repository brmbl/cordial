<?php

namespace App\ValueObjects;


class Password
{
    const MIN_LENGTH = 3;

    /** @var string */
    private $hashed;

    /** @var string */
    private $plain;

    /**
     * @param string $plain
     */
    private function __construct(string $plain)
    {
        if (strlen($plain) < self::MIN_LENGTH) {
            throw new \InvalidArgumentException('Password should contain at least '
                . self::MIN_LENGTH . ' characters');
        }

        $hashedPassword = password_hash($plain, PASSWORD_DEFAULT);
        if (!$hashedPassword) {
            throw new \InvalidArgumentException("Cannot process password");
        }

        $this->hashed = $hashedPassword;
        $this->plain = $plain;
    }

    /**
     * @param string $plain
     *
     * @return Password
     */
    public static function fromPlain(string $plain): Password
    {
        return new self ($plain);
    }

    /**
     * @return string
     */
    public function getHashed(): string
    {
        return $this->hashed;
    }

    /**
     * @return string
     */
    public function getPlain(): string
    {
        return $this->plain;
    }

    /**
     * @param string $hash
     *
     * @return bool
     */
    public function validateByHash(string $hash): bool
    {
        return password_verify($this->plain, $hash);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->hashed;
    }
}
