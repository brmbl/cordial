<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class ActivationCode extends Model
{
    const STATUS_NEW = 'new';

    const STATUS_VERIFIED = 'verified';
    
    protected $connection = 'mongodb';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'code',
        'status',
        'expire_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
