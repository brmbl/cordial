<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class EmailQueue extends Model
{
    const STATUS_FREE = 'FREE';

    const STATUS_IN_PROGRESS = 'IN_PROGRESS';

    const STATUS_DONE = 'DONE';

    const ERROR = 'ERROR';

    const TYPE_ACTIVATION = 'activation';

    /** @var string */
    protected $connection = 'mongodb';

    /** @var string */
    protected $table = 'emails_queue';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'recipient',
        'template',
        'params',
        'type',
        'status',
        'error_message'
    ];
}
