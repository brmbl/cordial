<?php

namespace App\Exceptions;


class ValidationException extends \Exception
{
    /** @var InvalidParameterException[] */
    private $errors;

    /**
     * @param array $errors
     */
    public function __construct(array $errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return InvalidParameterException[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
