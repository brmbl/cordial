<?php

namespace App\Exceptions;


class InvalidParameterException extends \Exception
{
    /** @var string */
    private $parameterName;

    /** @var string */
    private $errorMessage;

    /**
     * @param string $parameterName
     * @param string $errorMessage
     */
    public function __construct(string $parameterName, string $errorMessage)
    {
        $this->parameterName = $parameterName;
        $this->errorMessage = $errorMessage;

        parent::__construct($errorMessage, 400);
    }

    /**
     * @return string
     */
    public function getParameterName(): string
    {
        return $this->parameterName;
    }
}
