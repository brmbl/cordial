<?php

namespace App\Workers;

use App\EmailQueue;
use App\Mail\MailFactory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;

class EmailWorker
{
    /**
     * @param int $batch
     */
    public function handle(int $batch = 10): void
    {
        /** @var EmailQueue[] $emailQueueItems */
        $emailQueueItems = EmailQueue::where('status', EmailQueue::STATUS_FREE)->take($batch)->get();
        $this->lock($emailQueueItems);
        foreach ($emailQueueItems as $emailQueueItem) {
            try {
                $recipient = $emailQueueItem->getAttribute('recipient');
                $email = MailFactory::createFromQueue($emailQueueItem);
                Mail::to($recipient)->send($email);
                $this->markAsDone($emailQueueItem);
            } catch (\Throwable $e) {
                $this->markAsError($emailQueueItem, $e);
            }
        }
    }

    /**
     * @param Collection $emailQueueItems
     */
    private function lock(Collection $emailQueueItems): void
    {
        foreach ($emailQueueItems as $emailQueueItem) {
            $emailQueueItem->status = EmailQueue::STATUS_IN_PROGRESS;
            $emailQueueItem->save();
        }
    }

    /**
     * @param EmailQueue $emailQueueItem
     */
    private function markAsDone(EmailQueue $emailQueueItem): void
    {
        $emailQueueItem->status = EmailQueue::STATUS_DONE;
        $emailQueueItem->save();
    }

    /**
     * @param EmailQueue $emailQueueItem
     * @param \Throwable $e
     */
    private function markAsError(EmailQueue $emailQueueItem, \Throwable $e): void
    {
        $emailQueueItem->status = EmailQueue::ERROR;
        $emailQueueItem->error_message = $e->getMessage();
        $emailQueueItem->save();
    }
}
