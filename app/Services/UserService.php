<?php

namespace App\Services;

use App\ActivationCode as ActivationCodeModel;
use App\EmailQueue;
use App\Mail\ActivationCodeMail;
use App\ValueObjects\ActivationCode as ActivationCode;
use App\AuthToken;
use App\DTO\LoginData;
use App\DTO\RegistrationData;
use App\User;

class UserService
{
    /**
     * @param RegistrationData $registrationData
     *
     * @return string
     */
    public function register(RegistrationData $registrationData): string
    {
        $duplicate = User::where('email', (string)$registrationData->getEmail())->first();

        if ($duplicate) {
            throw new \LogicException('User with such email is already exists');
        }

        $user = new User();
        $user->fill([
            'name' => (string)$registrationData->getName(),
            'email' => (string)$registrationData->getEmail(),
            'password' => (string)$registrationData->getPassword(),
            'status' => User::STATUS_NEW
        ])->save();

        $code = ActivationCode::generate();
        $activationCode = (new ActivationCodeModel())->fill([
            'code' => (string)$code,
            'status' => ActivationCodeModel::STATUS_NEW,
            'expire_at' => (new \DateTime())->modify('+15 minutes')->format('d.m.y H:i:s')
        ]);
        $user->activationCode()->save($activationCode);

        (new EmailQueue())->fill([
            'recipient' => $user->getAttribute('email'),
            'template' => ActivationCodeMail::TEMPLATE_DEFAULT,
            'params' => json_encode(['code' => (string)$code]),
            'type' => EmailQueue::TYPE_ACTIVATION,
            'status' => EmailQueue::STATUS_FREE
        ])->save();

        return $user->getAttribute('id');
    }

    /**
     * @param LoginData $loginData
     *
     * @return string
     */
    public function login(LoginData $loginData): string
    {
        $user = User::where('email', (string)$loginData->getEmail())->first();
        if (!$user instanceof User) {
            throw new \LogicException('User with such email does not exists');
        }

        if (!$loginData->getPassword()->validateByHash($user->password)) {
            throw new \LogicException('Wrong password');
        }

        $token = base64_encode(uniqid());
        $authToken = new AuthToken();
        $authToken->fill([
            'token' => $token
        ]);
        $user->authTokens()->save($authToken);

        return $token;
    }
}
