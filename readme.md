# Simple API with Lumen and mongoDB

## Installation

- Clone repository
- Run ```composer install```  
- Run ```docker-compose up -d```


## API Doc
Api doc available on root page ```localhost:8888```

##Email sending
To make email sending works, you have to put your gmail credentials to `.env`  
Fields `MAIL_USERNAME` and `MAIL_PASSWORD`
