<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Api Doc</title>
    <style>
        table {
            border-collapse: collapse;
        }

        th {
            background: #ccc;
        }

        th, td {
            border: 1px solid #ccc;
            padding: 8px;
        }

        tr:nth-child(even) {
            background: #efefef;
        }

        tr:hover {
            background: #d1d1d1;
        }
    </style>
</head>
<body>
<table>
    <thead class="foldable">
    <tr>
        <th>Method</th>
        <th>Url</th>
        <th>Request params</th>
        <th>Response</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td>POST</td>
            <td>/registration</td>
            <td>Request body: <br> {
                "name": "Jon Doe",
                "email": "some@test.mail",
                "password": "secret"
                }
            </td>
            <td>HTTP 201 OK <br> {"id":"5d05860e7a69110006504493"}</td>
        </tr>
        <tr>
            <td>POST</td>
            <td>/login</td>
            <td>Request body: <br> {
                "email": "some@test.mail",
                "password": "secret"
                }
            </td>
            <td>HTTP 200 OK <br> {"auth_token":"NWQwNTg4MDBlYmM2MQ=="}</td>
        </tr>
        <tr>
            <td>GET</td>
            <td>/restricted</td>
            <td>Header: <br>
                "auth-token": "NWQwNTg4MDBlYmM2MQ=="
            </td>
            <td>HTTP 200 OK </td>
        </tr>
    </tbody>
</table>
</body>
</html>
